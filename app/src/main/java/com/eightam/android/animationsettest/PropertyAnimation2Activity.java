package com.eightam.android.animationsettest;

import android.animation.Animator;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class PropertyAnimation2Activity extends Activity {

    protected View testView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);

        testView = findViewById(R.id.view);
        testAnimate();
    }

    private void testAnimate() {
        testView.setTranslationX(-100);
        testView.setAlpha(0.0f);

        testView.animate()
                .translationX(100)
                .alpha(1.0f)
                .setDuration(2000)
                .setListener(new AnimatorListener1())
                .start();


    }

    class AnimatorListener1 extends CancellationTrackingAnimatorListener {

        @Override
        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);

            if (!isCancelled()) {
                testView.animate()
                        .translationX(-100)
                        .alpha(0.0f)
                        .setListener(new AnimatorListener2())
                        .start();
            }
        }

    }

    class AnimatorListener2 extends CancellationTrackingAnimatorListener {

        @Override
        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);

            if (!isCancelled()) {
                testAnimate();
            }
        }

    }

}
