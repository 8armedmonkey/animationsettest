package com.eightam.android.animationsettest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity implements View.OnClickListener {

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonPropertyAnimation1:
                startActivity(new Intent(this, PropertyAnimation1Activity.class));
                break;

            case R.id.buttonPropertyAnimation2:
                startActivity(new Intent(this, PropertyAnimation2Activity.class));
                break;

            case R.id.buttonOldSchoolAnimation:
                startActivity(new Intent(this, OldSchoolAnimationActivity.class));
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.buttonPropertyAnimation1).setOnClickListener(this);
        findViewById(R.id.buttonPropertyAnimation2).setOnClickListener(this);
        findViewById(R.id.buttonOldSchoolAnimation).setOnClickListener(this);
    }


}
