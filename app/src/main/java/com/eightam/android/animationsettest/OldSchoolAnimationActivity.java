package com.eightam.android.animationsettest;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;

public class OldSchoolAnimationActivity extends Activity {

    protected View testView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);

        testView = findViewById(R.id.view);
        testAnimate();
    }

    private void testAnimate() {
        final AnimationSet animation1 = new AnimationSet(false);
        animation1.addAnimation(new TranslateAnimation(100, -100, 0, 0));
        animation1.addAnimation(new AlphaAnimation(0.0f, 1.0f));
        animation1.setDuration(2000);
        animation1.setFillBefore(true);
        animation1.setFillAfter(true);

        final AnimationSet animation2 = new AnimationSet(false);
        animation2.addAnimation(new TranslateAnimation(-100, 100, 0, 0));
        animation2.addAnimation(new AlphaAnimation(1.0f, 0.0f));
        animation2.setDuration(2000);
        animation2.setFillBefore(true);
        animation2.setFillAfter(true);

        animation1.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                testView.startAnimation(animation2);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

        });

        animation2.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                testView.startAnimation(animation1);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

        });

        testView.startAnimation(animation1);
    }

}
