package com.eightam.android.animationsettest;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class PropertyAnimation1Activity extends Activity {

    protected View testView;
    protected AnimatorSet testAnimator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);

        testView = findViewById(R.id.view);
        testAnimate();
    }

    private void testAnimate() {
        if (testAnimator != null) {
            testAnimator.cancel();
        }

        testAnimator = new AnimatorSet();

        AnimatorSet animation1 = new AnimatorSet();
        AnimatorSet animation2 = new AnimatorSet();

        animation1.playTogether(
                ObjectAnimator.ofFloat(testView, View.TRANSLATION_X, -100, 100),
                ObjectAnimator.ofFloat(testView, View.ALPHA, 0.0f, 1.0f)
        );

        animation1.setDuration(2000);

        animation2.playTogether(
                ObjectAnimator.ofFloat(testView, View.TRANSLATION_X, 100, -100),
                ObjectAnimator.ofFloat(testView, View.ALPHA, 1.0f, 0.0f)
        );

        animation2.setDuration(2000);

        testAnimator.playSequentially(animation1, animation2);
        testAnimator.addListener(new CancellationTrackingAnimatorListener() {

            @Override
            public void onAnimationEnd(Animator animation) {
                if (!isCancelled()) {
                    animation.start();
                }
            }

        });

        testAnimator.start();
    }

}
